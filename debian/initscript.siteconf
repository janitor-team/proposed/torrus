#####################################################################
# This file may define some variables, controlling the initialization
# of Torrus daemons via /etc/init.d/torrus-common script.
# It also contains configuration data for the rrdup_notify script that
# may be run from cron.
# If a variable is not defined, the default value will be assumed.
# To modify the value, uncomment the line with the variable definition
# and edit it.
#####################################################################
# Daemons to start. By default an attempt is made to start both
# collector and monitor daemons for all configured trees. The
# per-tree customization may be achieved by modifying the
# tree configuration in /etc/torrus/conf/torrus-siteconfig.pl
#
# TORRUS_DAEMONS='collector monitor'
#####################################################################
# Extra options which will be supplied to all daemons when
# launching them, like '--verbose' or '--debug', for example.
#
# TORRUS_CMDOPTS=''
#####################################################################
# These variables control how the Torrus daemons are stopped. When
# the initialization script is called with 'stop' argument, all
# active Torrus daemon processes will be sent a TERM signal. After
# that it will sleep for ${TORRUS_KILL_SLEEP} seconds and check,
# whether the daemons have actually terminated. If not, it will
# repeat the procedure of sending a TERM signal and sleeping for
# ${TORRUS_KILL_SLEEP} seconds until either the daemons exit or 
# ${TORRUS_KILL_COUNT} attempts to stop them have been made. After
# that the remaining processes will be sent the KILL signals and their
# PID files will be unconditionally removed. Note that in the
# worst case scenario (using the default values) you might need 
# to wait up to 100 seconds for this procedure to finish.
#
# TORRUS_KILL_COUNT='9'
# TORRUS_KILL_SLEEP='10'
#####################################################################
# This variable controls the amount of details the init script
# outputs while stopping the Torrus daemons. Default setting is
# 'no', which causes it to output just one informational message. 
# Setting it to 'yes' causes the details about signal sending, number
# of retries and progress bars during waiting periods to be displayed.
#
# TORRUS_VERBOSE='no'
#####################################################################
# This variable implements a default delay of 20 minutes before
# starting the monitor daemons, giving the collector daemon a 
# chance to actually collect some data, before starting the
# monitoring. The value of this variable (if non-empty) is appended
# as an option to the invocation of the monitor daemon. 
#
# TORRUS_MONITOR_DELAY='--delay=20'
#####################################################################
# This variable allows to pass extra options to the collector daemon
# only.
#
# TORRUS_COLLECTOR_CMDOPTS=''
#####################################################################
# If this variable is set, rrdup_notify runs and sends any findings
# to the mail address set here.
#
# RRDUP_N_NOTIFY=root
#####################################################################
# Where the RRD files are located. Separate multiple paths with space
#
# RRDUP_N_RRDSTORAGE=/var/lib/torrus/collector_rrd
#####################################################################
# Maximum allowed age of an RRD file, in minutes
#
# RRDUP_N_MAXAGE=60
#####################################################################
