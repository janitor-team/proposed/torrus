torrus (3.00-2.0) UNRELEASED; urgency=medium

  [ Marc Haber ]
  * NOT YET RELEASED

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/watch: Use https protocol

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 11 Aug 2018 17:14:00 +0200

torrus (3.00-1) unstable; urgency=medium

  * New upstream version 3.00
    * drop 05_manpages_typo fixes patch, included upstream
    * refresh other patches
    * only call torrus clearcache from postinst if it exists
    * only chown/chmod /var/cache/torrus if it exists
    * adapt package dependencies
    * remove /var/lib/torrus/db
    * get rid of berkeley db hints and code
  * lintian override for add lintian override for torrus-common:
    init.d-script-needs-depends-on-lsb-base, we have a native
    systemd service
  * integrate NEWS and torrus-common.NEWS to a single file. Oops.
  * use a valid maintainer address. Closes: #899712
  * Accept patch to assist reproducible build.
    Thanks to Clint Adams. Closes: #847024

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 11 Aug 2018 16:23:56 +0200

torrus (2.09-2) unstable; urgency=medium

  * remove Jurij from uploaders, put Berni first.
    Thanks to Jurij Smakov, Mattia Rizzolo (Closes: #859873)
  * alioth => salsa:
    * move Vcs links to salsa
    * set torrus@packages.debian.org as maintainer
  * lintian:
    * don't do recursive chown in postinst
    * package is now priority: optional
    * remove superfluous override
  * Standards-Version: 4.1.4 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 12 May 2018 15:42:36 +0200

torrus (2.09-1) unstable; urgency=medium

  * New upstream version 2.09
    * Drop 20_fix_snmpv3_with_aes.patch, included upstream
    * Refresh 10_rrdup_notify.patch to match new upstream mail
    * add 05_fix_manpage_typos.patch to fix typos
    * Replace initscript with upstream one
      - add d/p/11_init_debian.patch for adjustments
  * Bump Standards to 3.9.8, no changes
  * use https for Vcs-Browser and Vcs-Git URL
  * Fix typos in torrus-common.doc-base.stylingprofile

 -- Bernhard Schmidt <berni@debian.org>  Thu, 17 Nov 2016 15:13:08 +0100

torrus (2.08-4) unstable; urgency=medium

  * set POD_MAN_DATE to build manpages (and thus hopefully the whole
    package) reproducibly

 -- Bernhard Schmidt <berni@debian.org>  Sun, 15 Nov 2015 20:26:32 +0100

torrus (2.08-3) unstable; urgency=medium

  * Cherry-pick upstream commit 9ba7f4ba to fix SNMPv3 with AES
    (Closes: #804173)
  * Update Vcs-Browser control field
  * use /nonexistent for adduser, fixing a lintian error

 -- Bernhard Schmidt <berni@debian.org>  Fri, 13 Nov 2015 21:07:02 +0100

torrus (2.08-2) unstable; urgency=medium

  * Revert broken patch refresh in commit 486f4baa which causes
    rrdup_notify to look into the wrong directory (Closes: #774851)

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Tue, 02 Jun 2015 14:25:53 +0200

torrus (2.08-1) unstable; urgency=medium

  * Upload to unstable

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Sun, 12 Oct 2014 21:16:07 +0200

torrus (2.08-1~exp1) experimental; urgency=medium

  [ Bernhard Schmidt ]
  * Imported Upstream version 2.08
  * drop debian/patches/05_doc-stylingprofile-podfix and
    debian/patches/06_doc-xmlconfig-podfix, applied upstream

  [ Christoph Berg ]
  * Remove myself from Uploaders.

  [ Marc Haber ]
  * Extend BerkeleyDB recover procedure docs, from a torrus-devel msg
    from Berni from 2011.

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Sun, 05 Oct 2014 21:16:04 +0200

torrus (2.07-2) unstable; urgency=low

  [ Bernhard Schmidt ]
  * Fix POD syntax errors in stylingprofile.pod.in and xmlconfig.pod.in
    (Closes: #725598)

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Mon, 07 Oct 2013 16:56:04 +0200

torrus (2.07-1) unstable; urgency=low

  [ Bernhard Schmidt ]
  * Imported Upstream version 2.07
    * refresh patches
    * drop 99_log_sighup.patches, applied upstream
  * bump debhelper compat level to 9
  * bump standards version to 3.9.4, no changes needed
  * Remove obsolete torrus-apache2 package, upstream only supports the
    FastCGI interface in torrus-common these days (Closes: #669791)
    * torrus-common replaces torrus-apache2
    * add one last hint in torrus-common.NEWS
    * remove torrus-apache2.conf symlink in torrus-common.postinst

 -- Bernhard Schmidt <berni+deb@birkenwald.de>  Wed, 06 Jun 2013 17:24:30 +0200

torrus (2.03-2) unstable; urgency=low

  [ Bernhard Schmidt ]
  * set torrus_user correctly for configure (Closes: #686099)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 19 May 2012 22:42:20 +0200

torrus (2.03-1) unstable; urgency=low

  [ Bernhard Schmidt ]
  * Imported Upstream version 2.03
    * now logs through syslog. Closes: #581028
    * now dies on SIGHUP. Closes: #579878
  * Update README.Debian, describe apache2-mod-fcgid configuration
  * Add torrus-apache2.NEWS to notify users to migrate to FastCGI
  * Remove logrotate support, Torrus 2.02 has switched to syslog and
    dies on SIGHUP
  * Cherry-pick upstream commit 66bb417 to log exit on SIGHUP

  [ Marc Haber ]
  * Standards-Version: 3.9.3 (no changes needed)
  * init script: ignore exit code from expr
  * store package without patches applied
  * README: fix wrong package name
  * disable user authentication.
    Thanks to Mark Lawrence (Closes: #488784)
  * refresh patches
  * do not call torrus acl in postinst
  * give an example for localhost configuration

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Wed, 16 May 2012 22:58:29 +0200

torrus (2.02-1) UNRELEASED; urgency=low

  [Bernhard Schmidt]
  * Imported Upstream version 2.02
    * logs to syslog by default (Closes: #581028)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 15 Apr 2012 20:43:42 +0000

torrus (2.01-3) unstable; urgency=low

  * Add liburi-perl to Depends, spotted by piuparts/Andreas Beckmann.
    (Closes: #656120)
  * Remove libdigest-sha1-perl from Description, spotted by Salvatore
    Bonaccorso. (Closes: #656765)
  * Also run make clean in debian/html.
  * Add Bernhard as Uploader.

 -- Christoph Berg <myon@debian.org>  Tue, 24 Jan 2012 10:00:22 +0100

torrus (2.01-2) unstable; urgency=low

  * upload to unstable

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Thu, 17 Nov 2011 21:42:13 +0100

torrus (2.01-1) experimental; urgency=low

  * The first "Bernie to the rescue" release

  [ Christoph Berg ]
  * Remove transitional package torrus-apache.

  [ Bernhard Schmidt ]
  * Imported Upstream version 2.01 (Closes: #603590)
  * move xmlconfig examples to /usr/share/doc (Closes: #579872)
  * run db_recover in postinst (Closes: #476356)
  * convert package to dh7
  * convert package to source format 3.0(quilt), drop dpatch
  * change Vcs-Fields to new git repository
  * Standards-Version: 3.9.2 (no changes necessary)

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 06 Nov 2011 17:15:40 +0100

torrus (1.0.8-2) unstable; urgency=low

  [ Marc Haber ]
  * Make torrus conflict with apache2-mpm-worker and apache2-mpm-event.
    Thanks to Stanislav Sinyagin. Closes: #571139
  * Add logrotate stanza for dbenv_errlog.
    Thanks to Jörg Dorchain. Closes: #497031
  * make lintian happy
    * fix typo in doc-base file
    * Standards-Version: 3.8.4 (no changes necessary)
    * add debian/README.Source
    * add ${misc:Depends} to binary Depends
    * update autotools helper files (code from
      /usr/share/doc/autotools-dev/examples/rules.gz)
    * build-depend on autotools-dev

  [ Christoph Berg ]
  * Use dh_md5sums.

 -- Christoph Berg <myon@debian.org>  Fri, 26 Mar 2010 11:09:41 +0100

torrus (1.0.8-1) unstable; urgency=low

  [ Marc Haber ]
  * Fix wrong logic in /lib/lsb/init-functions processing.
  * Document the need to db_recover after libberkeleydb-perl update.
    Thanks to Jörg Dorchain. This partly addresses #476356

  [ Christoph Berg ]
  * New upstream version 1.0.8. Closes: #497502
  * Adding myself as Uploader, thanks Marc. Closes: #433223
  * Create /var/run/ from init script instead of shipping it in the deb.
  * Use invoke-rc.d in init script.
  * Maintainer scripts use -e, call apache2ctl without explicit path.
  * Update doc-base section to Network/Monitoring.
  * Bumping to DH level 5, and Standards-Version to 3.8.1.
  * Adding Homepage and Vcs control headers and watch file.

 -- Christoph Berg <myon@debian.org>  Thu, 14 May 2009 16:33:08 +0200

torrus (1.0.6-2) unstable; urgency=low

  * Add torrus-common.NEWS advising people to recompile their
    configuration upon upgrading to torrus 1.0.6.
    Thanks to Joerg Dorchain. Closes: #469274

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 08 Mar 2008 00:18:46 +0100

torrus (1.0.6-1) unstable; urgency=low

  * New upstream version
  * Remove torrus-apache conffiles on torrus-apache purge.
    Thanks to Kumar Appaiah and the piuparts team. Closes: #454389
  * Init script:
    * add LSB Headers
    * adapt semantics to LSB, output and status target. Closes: #377033
  * Make lintian happy
    * Add overrides for empty /usr/share directories
    * Add descriptions to our dpatches
    * Standards-Version: 3.7.3 (no changes necessary)
  * Streamline patch names
  * allow rrdup_notify to be configured from /etc/default/torrus-common,
    install rrdup_notify to /usr/share/torrus, invoke from cron.daily.
    Closes: #380625
  * torrus-apache2.postinst: give a more meaningful error message if
    apache2ctl configtest fails

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 16 Feb 2008 21:36:01 +0100

torrus (1.0.5-2) unstable; urgency=low

  * Check for /usr/sbin/torrus in cron job.
    Thanks to Filippo Giunchedi. Closes: #436021
  * Remove doc/*.txt in the clean target to get *.pod files
    regenerated correctly on second and subsequent builds.
    Closes: #441729

 -- Jurij Smakov <jurij@debian.org>  Sat, 15 Sep 2007 18:34:10 +0100

torrus (1.0.5-1) unstable; urgency=low

  * New upstream release. Closes: #409206
  * remove apache support. Closes: #429070
  * Mailing list as Maintainer. We're looking for help! See #433223
  * Update debian/patches/03-doc-userguide.dpatch, so that it
    applies cleanly to the new version.
  * README.Debian: apache/conf.d is in fact apache2/conf.d.
    Thanks to Christopher Huhn. Closes: #407448
  * Use Source:Version instead of Source-Version.
  * Remove gratuitous -ptorrus-common parameters
  * debian/rules: replace docdir with pkgdocdir in configure call.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 28 Jul 2007 22:53:30 +0200

torrus (1.0.4-1) unstable; urgency=low

  [ Upstream changes relevant for Debian ]
  * New upstream release.

  [ Jurij Smakov ]
  * Expand the copyright file to include the information about all
    authors and copyright holders.
  * Use invoke-rc.d instead of invoking the init script directly
    in maintainer scripts.

  [ Marc Haber ]
  * Standards-Version: 3.7.2 (no changes needed)
  * Move debhelper and dpatch to Build-Depends.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Mon, 24 Jul 2006 13:08:45 +0000

torrus (1.0.3-1) unstable; urgency=low

  [ Upstream changes relevant for Debian ]
  * New upstream release.

  [ Jurij Smakov ]
  * Removed dependency of torrus-apache2 on libapache2-mod-apreq2,
    which is normally pulled in by libapache2-request-perl, but due
    to a bug in the latter (#319614) that was not happening, so an
    explicit dep was required. Thanks to Steinar H. Gunderson for
    clarification.
  * Added a version (>= 2.05-dev-6) to the libapache2-request-perl
    to guarantee that we have the version in which the above bug is
    fixed, and ensure that the version of libapreq2 (on which
    libapache2-request-perl depends) is at least 2.05, as required
    by upstream.

 -- Jurij Smakov <jurij@wooyd.org>  Wed,  3 Aug 2005 20:02:19 -0400

torrus (1.0.2-1) unstable; urgency=low

  [ Upstream changes relevant for Debian ]
  * New upstream release.
  * Fixed Apache2 support, broken by mod_perl2 API change. Minimum
    supported mod_perl2 version is now 2.0.0.
    Closes: #318847
  * Fixed the breakage caused by removal of the cache files in
    /var/cache/torrus. The absence of cache files is now detected
    and they are regenerated on the fly, in compliance with FHS.
    Closes: #318892

  [ Marc Haber ]
  * Removed "unreleased" warnings from debian/control. duh.

  [ Jurij Smakov ]
  * Included the description of a new TORRUS_COLLECTOR_CMDOPTS
    variable in /etc/torrus/initscript.siteconf and updated
    the init script to honour it.
  * Bumped Standards-Version to 3.6.2.
  * As per new policy recommendation, all user documentation
    (except migration and installation guides, which are irrelevant
    for Debian) is now registered with doc-base.
  * New dependency on libtimedate-perl which provides Date::Parse
    and Date::Format, per upstream requirement.
  * Bumped the minimum version of libapache2-mod-perl2 providing
    mod_perl2 to 2.0.0 per upstream requirement.
  * Removed dependencies on adduser for torrus-apache and
    torrus-apache2 and added it to torrus-common, since adduser is
    only used in torrus-common postinst.
  * Added --no-create-home option to adduser call in torrus-common
    postinst.
  * Added dependency on libapache2-mod-apreq2 for torrus-apache2
    since it provides the Apache2 module, earlier shipped as a
    part of libapreq2 package.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sat, 30 Jul 2005 18:53:20 +0000

torrus (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * Move init script defaults to /etc/torrus/conf/initscript.siteconf,
    getting the package in line with upstream. For compatibility, we
    symlink to /etc/default/torrus-common.
    (Jurij Smakov).

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 12 Jun 2005 21:07:22 +0000

torrus (0.99.0.20050603-1) experimental; urgency=low

  * Moved rrdup_notify.sh to doc/examples, it needs to be edited before
    being useful (Marc Haber).
  * Corrected a typo in torrus-apache description, incorrectly stating
    that support for Apache 2.x is provided by torrus-apache (should
    be torrus-apache2) (Jurij Smakov). Closes: #306992
  * Documentation in .txt format is included as upstream builds it now.
  * Added a manpage for a new command ttproclist, will submit
    upstream for inclusion (Jurij Smakov).

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Fri,  3 Jun 2005 23:38:21 +0000

torrus (0.99.0.20040410-1) experimental; urgency=low

  * First release in Debian. Closes: #186828
  * This is a packaged upstream snapshot. Since it is not an upstream
    release, this package is meant for experimental to allow testing in
    Debian.
  * Package by Jurij Smakov, sponsored by Marc Haber.

 -- Marc Haber <mh+debian-packages@zugschlus.de>  Sun, 24 Apr 2005 18:24:34 +0000
